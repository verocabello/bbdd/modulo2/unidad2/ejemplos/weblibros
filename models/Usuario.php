<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property string $login
 * @property int $contraseña
 * @property string $correo
 * @property string $fechaalta
 *
 * @property Descargar[] $descargars
 * @property Libro[] $ids
 */
class Usuario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['contraseña'], 'integer'],
            [['fechaalta'], 'safe'],
            [['login'], 'string', 'max' => 20],
            [['correo'], 'string', 'max' => 50],
            [['login'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Login',
            'contraseña' => 'Contraseña',
            'correo' => 'Correo',
            'fechaalta' => 'Fechaalta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescargars()
    {
        return $this->hasMany(Descargar::className(), ['login' => 'login']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIds()
    {
        return $this->hasMany(Libro::className(), ['id' => 'id'])->viaTable('descargar', ['login' => 'login']);
    }
}
