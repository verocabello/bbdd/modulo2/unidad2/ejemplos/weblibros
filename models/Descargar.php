<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "descargar".
 *
 * @property int $iddescarga
 * @property int $id
 * @property string $login
 *
 * @property Libro $id0
 * @property Usuario $login0
 * @property Fechadescarga[] $fechadescargas
 */
class Descargar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'descargar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['login'], 'string', 'max' => 20],
            [['id', 'login'], 'unique', 'targetAttribute' => ['id', 'login']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Libro::className(), 'targetAttribute' => ['id' => 'id']],
            [['login'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['login' => 'login']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddescarga' => 'Iddescarga',
            'id' => 'ID',
            'login' => 'Login',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Libro::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogin0()
    {
        return $this->hasOne(Usuario::className(), ['login' => 'login']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFechadescargas()
    {
        return $this->hasMany(Fechadescarga::className(), ['iddescarga' => 'iddescarga']);
    }
}
