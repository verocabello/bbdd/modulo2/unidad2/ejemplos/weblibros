<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libro".
 *
 * @property int $autorescribe
 * @property int $id
 * @property string $titulo
 *
 * @property Ayuda[] $ayudas
 * @property Autor[] $autorayudas
 * @property Coleccion[] $coleccions
 * @property Descargar[] $descargars
 * @property Usuario[] $logins
 * @property Autor $autorescribe0
 */
class Libro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['autorescribe'], 'integer'],
            [['titulo'], 'string', 'max' => 50],
            [['autorescribe'], 'exist', 'skipOnError' => true, 'targetClass' => Autor::className(), 'targetAttribute' => ['autorescribe' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'autorescribe' => 'Escritor',
            'id' => 'ID',
            'titulo' => 'Titulo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAyudas()
    {
        return $this->hasMany(Ayuda::className(), ['libro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutorayudas()
    {
        return $this->hasMany(Autor::className(), ['id' => 'autorayuda'])->viaTable('ayuda', ['libro' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getColeccions()
    {
        return $this->hasMany(Coleccion::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescargars()
    {
        return $this->hasMany(Descargar::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogins()
    {
        return $this->hasMany(Usuario::className(), ['login' => 'login'])->viaTable('descargar', ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutorescribe0()
    {
        return $this->hasOne(Autor::className(), ['id' => 'autorescribe']);
    }
}
