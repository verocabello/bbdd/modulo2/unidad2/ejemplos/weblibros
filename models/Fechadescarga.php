<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fechadescarga".
 *
 * @property int $idfechadescarga
 * @property int $iddescarga
 * @property string $fechadescarga
 *
 * @property Descargar $descarga
 */
class Fechadescarga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fechadescarga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddescarga'], 'integer'],
            [['fechadescarga'], 'safe'],
            [['iddescarga', 'fechadescarga'], 'unique', 'targetAttribute' => ['iddescarga', 'fechadescarga']],
            [['iddescarga'], 'exist', 'skipOnError' => true, 'targetClass' => Descargar::className(), 'targetAttribute' => ['iddescarga' => 'iddescarga']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idfechadescarga' => 'Idfechadescarga',
            'iddescarga' => 'Iddescarga',
            'fechadescarga' => 'Fechadescarga',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescarga()
    {
        return $this->hasOne(Descargar::className(), ['iddescarga' => 'iddescarga']);
    }
}
