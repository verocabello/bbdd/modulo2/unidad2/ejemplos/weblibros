<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ayuda".
 *
 * @property int $idayuda
 * @property int $libro
 * @property int $autorayuda
 *
 * @property Autor $autorayuda0
 * @property Libro $libro0
 */
class Ayuda extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ayuda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['libro', 'autorayuda'], 'integer'],
            [['libro', 'autorayuda'], 'unique', 'targetAttribute' => ['libro', 'autorayuda']],
            [['autorayuda'], 'exist', 'skipOnError' => true, 'targetClass' => Autor::className(), 'targetAttribute' => ['autorayuda' => 'id']],
            [['libro'], 'exist', 'skipOnError' => true, 'targetClass' => Libro::className(), 'targetAttribute' => ['libro' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idayuda' => 'Idayuda',
            'libro' => 'Libro',
            'autorayuda' => 'Autorayuda',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutorayuda0()
    {
        return $this->hasOne(Autor::className(), ['id' => 'autorayuda']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibro0()
    {
        return $this->hasOne(Libro::className(), ['id' => 'libro']);
    }
}
