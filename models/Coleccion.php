<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coleccion".
 *
 * @property int $idcoleccion
 * @property int $id
 * @property string $coleccion
 *
 * @property Libro $id0
 */
class Coleccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coleccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['coleccion'], 'string', 'max' => 50],
            [['id', 'coleccion'], 'unique', 'targetAttribute' => ['id', 'coleccion']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Libro::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcoleccion' => 'Idcoleccion',
            'id' => 'ID',
            'coleccion' => 'Coleccion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId0()
    {
        return $this->hasOne(Libro::className(), ['id' => 'id']);
    }
}
