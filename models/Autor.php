<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autor".
 *
 * @property int $id
 * @property string $nombre
 * @property string $fechanacimiento
 *
 * @property Ayuda[] $ayudas
 * @property Libro[] $libros
 * @property Libro[] $libros0
 */
class Autor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fechanacimiento'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'fechanacimiento' => 'Fechanacimiento',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAyudas()
    {
        return $this->hasMany(Ayuda::className(), ['autorayuda' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros()
    {
        return $this->hasMany(Libro::className(), ['id' => 'libro'])->viaTable('ayuda', ['autorayuda' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLibros0()
    {
        return $this->hasMany(Libro::className(), ['autorescribe' => 'id']);
    }
}
