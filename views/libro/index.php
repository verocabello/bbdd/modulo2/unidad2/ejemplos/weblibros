<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Libros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="libro-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Libro', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'titulo',
            'autorescribe',
            'autorescribe0.nombre',
            [
            'label'=>'Nombre del escritor',
            'format'=>'raw',
            'value' => function($registro){
                return Html::a($registro->autorescribe0->nombre,['autor/view_1','id'=>$registro->autorescribe]);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete} {ver}',
            'buttons' => [
                'ver' => function ($url,$registro,$key) {
                    return Html::a('<span class="glyphicon glyphicon-user"></span>',['autor/view_1','id'=>$registro->autorescribe]);
                },
	        ],
        ],    
                
                
        ],
    ]); ?>


</div>
