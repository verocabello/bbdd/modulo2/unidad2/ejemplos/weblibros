<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Descargar */

$this->title = 'Update Descargar: ' . $model->iddescarga;
$this->params['breadcrumbs'][] = ['label' => 'Descargars', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddescarga, 'url' => ['view', 'id' => $model->iddescarga]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="descargar-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
