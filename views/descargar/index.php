<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Descargars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="descargar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Descargar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'iddescarga',
            'id',
            'login',
            'login0.correo',
            
                [
            'label'=>'Email',
            'format'=>'raw',
            'value' => function($registro){
                return Html::a($registro->login0["correo"],['usuario/view_1','id'=>$registro->login]);
            }
        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
