<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ayuda */

$this->title = 'Create Ayuda';
$this->params['breadcrumbs'][] = ['label' => 'Ayudas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ayuda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
