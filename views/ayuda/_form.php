<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ayuda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ayuda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'libro')->textInput() ?>

    <?= $form->field($model, 'autorayuda')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
