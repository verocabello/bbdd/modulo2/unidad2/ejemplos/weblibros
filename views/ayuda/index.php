<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ayudas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ayuda-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ayuda', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'idayuda',
            'libro',
            'libro0.titulo',
            'autorayuda0.nombre',
             [
            'label'=>'Nombre del ayudante',
            'format'=>'raw',
            'value' => function($registro){
                return Html::a($registro->autorayuda0->nombre,['autor/view_2','id'=>$registro->autorayuda]);
            }
        ],
                 [
            'label'=>'Titulo del Libro',
            'format'=>'raw',
            'value' => function($registro){
                
                return Html::a($registro->libro0->titulo,['libro/view_1','id'=>$registro->libro]);
            }
        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
