<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fechadescarga */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechadescarga-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'iddescarga')->textInput() ?>

    <?= $form->field($model, 'fechadescarga')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
