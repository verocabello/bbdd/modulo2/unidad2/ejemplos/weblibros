<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fechadescarga */

$this->title = 'Create Fechadescarga';
$this->params['breadcrumbs'][] = ['label' => 'Fechadescargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechadescarga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
