<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fechadescarga */

$this->title = 'Update Fechadescarga: ' . $model->idfechadescarga;
$this->params['breadcrumbs'][] = ['label' => 'Fechadescargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idfechadescarga, 'url' => ['view', 'id' => $model->idfechadescarga]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fechadescarga-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
