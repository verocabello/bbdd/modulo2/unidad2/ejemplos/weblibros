<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coleccions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coleccion-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Coleccion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcoleccion',
            'id',
            'coleccion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
