<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Coleccion */

$this->title = 'Update Coleccion: ' . $model->idcoleccion;
$this->params['breadcrumbs'][] = ['label' => 'Coleccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcoleccion, 'url' => ['view', 'id' => $model->idcoleccion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="coleccion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
